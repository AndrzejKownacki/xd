const books = require('./books.js');
const _ = require('lodash');

const bookgenres = _ 
    .chain(books.booksArray)
    .flatMap(
        function(o) {
            if(typeof o.genre === "string"){
                return o
            } else {
                return _.map(o.genre, function(a){
                    return {...o, genre:a}
                })
            }
        }
    )
    .groupBy('genre')
    .value();

// bookgenres.sort((a,b) =>(a.))
const sorted_pairs = Object.entries(bookgenres).sort((a, b) => (a > b) ? 1 : -1)
const wynik = sorted_pairs.reduce((prev, curr) => {
    return {
        ...prev,
        [curr[0]]:
            curr[1].reduce((prev2, curr2) => {
                return [...prev2, {title: curr2.title, author: curr2.author}]
            }, [])
    }
}, {})


// console.log(_.map(books.booksArray[4].genre, e => e))
console.log(wynik)


// const result = books.booksArray.reduce((prev, curr) =>{
//     if(prev.includes(curr.genre)){
//         return prev
//     } else {
//         return [...prev, curr.genre]
//     }
// },[]) 

// const result = books.booksArray.reduce((prev, curr) =>{
//     if(curr.genre == "fantasy" || curr.genre[0] == "fantasy" || curr.genre[1] == "fantasy"){
//         const fantasy = _.map(curr, function(o){return{title: o.title, author:o.author}})
//         return [...prev, fantasy]
//     } else {
//         return prev
//     }
// },[])

// const result = books.booksArray.reduce((prev, curr) =>{
//     return curr.map()
// },[]) 

// console.log (result);